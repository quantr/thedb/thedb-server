CFLAGS=-g -Wall -Isrc -Ibin 
STATIC_LIBRARIES=/usr/local/lib/libuv.a
STATIC_LIBRARIES2=/usr/local/lib/libuv.so

all: bin cli  bin/thedb-server

bin:
	mkdir -p bin

cli: src/cli/cli.l src/cli/cli.y
	flex -o bin/lex.yy.c src/cli/cli.l
	bison -d src/cli/cli.y -o bin/cli.tab.c
	gcc -c bin/lex.yy.c -o bin/lex.yy.o
	gcc -c bin/cli.tab.c $(CFLAGS) -o bin/cli.tab.o
	gcc -c bin/cli.tab.c $(CFLAGS) -o bin/cli.tab.o

bin/argparse.o: src/argparse/argparse.c
	gcc -c $? $(CFLAGS) -o $@

bin/stringbuilder.o: src/stringbuilder/stringbuilder.c
	gcc -c $? $(CFLAGS) -o $@

bin/sysinfo.o: src/sysinfo/sysinfo.c
	gcc -c $? $(CFLAGS) -o $@

bin/structure.o: src/structure/structure.c
	gcc -c $? $(CFLAGS) -o $@
	
bin/doubleLinkedList.o: src/structure/doubleLinkedList.c
	gcc -c $? $(CFLAGS) -o $@

bin/hashmap.o: src/structure/hashmap.c
	gcc -c $? $(CFLAGS) -o $@

bin/dynamicString.o: src/structure/dynamicString.c
	gcc -c $? $(CFLAGS) -o $@

bin/command.o: src/command/command.c
	gcc -c $? $(CFLAGS) -o $@

bin/server.o: src/server/server.c
	gcc -c $? $(CFLAGS) -o $@

grammar: bin/thedbGrammar.tab.o bin/thedbGrammar.lex.o bin/thedbGrammerLib.o

bin/thedbGrammar.tab.o: src/grammar/thedbGrammar.y 
	bison -d src/grammar/thedbGrammar.y -o bin/thedbGrammar.tab.c
	gcc -c bin/thedbGrammar.tab.c $(CFLAGS) -o $@ 

bin/thedbGrammar.lex.o: src/grammar/thedbGrammar.l src/grammar/thedbGrammar.h
	flex -o bin/thedbGrammar.lex.c src/grammar/thedbGrammar.l
	gcc -c bin/thedbGrammar.lex.c $(CFLAGS) -o $@ 

bin/thedbGrammerLib.o: src/grammar/thedbGrammerLib.c
	gcc -c $? $(CFLAGS) -o $@ 

bin/thedb-server: bin/argparse.o bin/stringbuilder.o bin/sysinfo.o bin/command.o bin/structure.o  bin/dynamicString.o bin/hashmap.o grammar bin/server.o bin/thedbGrammar.tab.o bin/thedbGrammar.lex.o 
	gcc -pthread src/thedb-server.c $(CFLAGS) bin/stringbuilder.o bin/sysinfo.o bin/argparse.o bin/command.o bin/server.o bin/thedbGrammerLib.o bin/thedbGrammar.tab.o bin/structure.o  bin/dynamicString.o bin/hashmap.o bin/thedbGrammar.lex.o -o $@ -lm

bin/testGrammar: bin bin/argparse.o bin/stringbuilder.o bin/sysinfo.o bin/command.o bin/structure.o bin/doubleLinkedList.o bin/dynamicString.o bin/hashmap.o grammar bin/server.o bin/thedbGrammar.tab.o bin/thedbGrammar.lex.o src/grammar/testGrammar.c
	gcc -c src/grammar/testGrammar.c $(CFLAGS) -o bin/testGrammar.o
	gcc bin/hashmap.o bin/structure.o bin/dynamicString.o bin/testGrammar.o bin/thedbGrammar.tab.o bin/thedbGrammar.lex.o -o $@

bin/testMain: bin bin/stringbuilder.o  bin/structure.o bin/doubleLinkedList.o bin/dynamicString.o bin/hashmap.o src/structure/testMain.c
	gcc -c src/structure/testMain.c $(CFLAGS) -o bin/testMain.o
	gcc bin/hashmap.o bin/structure.o bin/dynamicString.o bin/testMain.o -o $@


clean:
	rm -fr bin
