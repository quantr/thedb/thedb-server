![](https://www.quantr.foundation/wp-content/uploads/2023/10/TheDB-english-no-HK-wide.png)

TheDB is a nosql database written in C

# Member

- Peter, System Architect <peter@quantr.hk>
- Miles, Programmer <miles@quantr.hk>
- Kenny, Programmer

# Dev notes

## VSCode setup

1. install plugins
2. edit any files in src, it will run "make" after save

```
Name: Trigger Task on Save
Id: Gruntfuggly.triggertaskonsave
Description: Run tasks when saving files
Version: 0.2.17
Publisher: Gruntfuggly
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.triggertaskonsave
```

```
Name: VSCode-YACC
Id: carlubian.yacc
Description: Provides support for YACC files within Visual Studio Code
Version: 1.3.1
Publisher: carlubian
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=carlubian.yacc
```

```
Lex Name: Lex
Id: luniclynx.lex
Description: Lex & Flex Syntax Highlighting
Version: 0.1.0
Publisher: luniclynx
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=luniclynx.lex
```

## Code format

We better align it

![](https://www.quantr.foundation/wp-content/uploads/2023/10/thedb-code-format.png)

## Project website and all doc

www.quantr.foundation

https://www.quantr.foundation/docs/?project=TheDB

# How to compile and run

```
sudo apt-get install libuv1-dev -y
make
./bin/thedb-server
```

# Our name

![](https://peter.quantr.hk/wp-content/uploads/2023/11/Why-call-TheDB.png)

# Roadmap

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
