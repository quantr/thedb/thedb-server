# TheDB Commands

## String
| Command           | Description                                                                      |
|-------------------|----------------------------------------------------------------------------------|
| str_append        | Appends a value to a string.                                                     |
| str_bitcount      | Counts the number of set bits in a string.                                       |
| str_bitop         | Performs bitwise operations on strings.                                          |
| str_bitpos        | Finds the position of the first set bit in a string.                             |
| str_decr          | Decrements an integer stored as a string.                                        |
| str_decrby        | Subtracts a value from an integer stored as a string.                            |
| str_get           | Retrieves the value of a key.                                                    |
| str_getbit        | Retrieves the bit value at a given index in a string.                            |
| str_getrange      | Retrieves a substring from a string.                                             |
| str_getset        | Sets a value and returns the old value stored at a key.                          |
| str_incr          | Increments an integer stored as a string.                                        |
| str_incrby        | Adds a value to an integer stored as a string.                                   |
| str_incrbyfloat   | Adds a floating-point value to a floating-point number stored as a string.       |
| str_mget          | Retrieves the values of multiple keys.                                           |
| str_mset          | Sets multiple key-value pairs.                                                   |
| str_msetnx        | Sets multiple key-value pairs only if the keys do not exist.                     |
| str_psetex        | Sets a value with an expiration time in milliseconds.                            |
| str_set           | Sets the value of a key.                                                         |
| str_setbit        | Sets or clears the bit at a given index in a string.                             |
| str_setex         | Sets a value with an expiration time in seconds.                                 |
| str_setnx         | Sets the value of a key only if it does not exist.                               |
| str_setrange      | Overwrites part of a string at a specified index.                                |
| str_strlen        | Returns the length of a string.                                                  |


## List
| Command              | Description                                                                                               |
|----------------------|-----------------------------------------------------------------------------------------------------------|
| list_blpop           | Removes and returns the leftmost element from a list, or blocks until an element is available.             |
| list_brpop           | Removes and returns the rightmost element from a list, or blocks until an element is available.            |
| list_brpoplpush      | Pops an element from the right end of one list and pushes it to the left end of another list, or blocks until an element is available. |
| list_index           | Retrieves an element from a list by its index.                                                            |
| list_insert          | Inserts an element next to a specified element in a list.                                                 |
| list_len             | Returns the length of a list.                                                                             |
| list_pop             | Removes and returns the leftmost element from a list.                                                     |
| list_push            | Inserts one or more elements at the left end of a list.                                                   |
| list_pushx           | Inserts an element at the left end of a list only if the list exists.                                     |
| list_range           | Retrieves a range of elements from a list.                                                                |
| list_rem             | Removes elements from a list.                                                                             |
| list_set             | Sets the value of an element in a list by its index.                                                      |
| list_trim            | Trims a list, keeping only the specified range of elements.                                               |
| list_rpop            | Removes and returns the rightmost element from a list.                                                    |
| list_rpoplpush       | Pops an element from the right end of a list and pushes it to the left end of the same list.              |
| list_rpush           | Inserts one or more elements at the right end of a list.                                                  |
| list_rpushx          | Inserts an element at the right end of a list only if the list exists.                                    |


## Set
| Command        | Description                                                                                |
| -------------- | ------------------------------------------------------------------------------------------ |
| set_add        | Add an item to a set.                                                                      |
| set_card       | Get the size of a set (number of elements).                                                |
| set_diff       | Get the difference between sets (elements present in the first set but not in the others). |
| set_diffstore  | Store the difference between sets into a new set.                                          |
| set_inter      | Get the intersection of sets (elements present in all sets).                               |
| set_interstore | Store the intersection of sets into a new set.                                             |
| set_ismember   | Check if an item exists in a set.                                                          |
| set_members    | Get all items in a set.                                                                    |
| set_move       | Move an item from one set to another.                                                      |
| set_pop        | Remove and return a random item from a set.                                                |
| set_randmember | Get a random item from a set.                                                              |
| set_rem        | Remove matching items from a set.                                                          |
| set_scan       | Iterate over items in a set.                                                               |
| set_union      | Get the union of sets (unique elements from all sets).                                     |
| set_unionstore | Store the union of sets into a new set.                                                    |

## Hash
| Command                | Description                                                                                           |
|------------------------|-------------------------------------------------------------------------------------------------------|
| hash_del               | Delete an item from a hash.                                                                           |
| hash_exists            | Check if an item exists in a hash.                                                                    |
| hash_get               | Get the value of an item in a hash.                                                                   |
| hash_getall            | Return all items in a hash.                                                                           |
| hash_incrby            | Increment an integer value in a hash.                                                                 |
| hash_incrbyfloat       | Increment a float value in a hash.                                                                    |
| hash_keys              | Return all keys in a hash.                                                                            |
| hash_len               | Get the number of items in a hash.                                                                    |
| hash_mget              | Get multiple items from a hash.                                                                       |
| hash_mset              | Set multiple items in a hash.                                                                         |
| hash_scan              | Iterate over items in a hash.                                                                         |
| hash_set               | Set an item in a hash.                                                                                |
| hash_setnx             | Set an item in a hash if it doesn't exist.                                                            |
| hash_vals              | Return all values in a hash.                                                                          |


## Sorted Set
| Command               | Description                                                                                                               |
|-----------------------|---------------------------------------------------------------------------------------------------------------------------|
| zset_add              | Add an item to a sorted set with a score.                                                                                 |
| zset_card             | Get the number of items in a sorted set.                                                                                  |
| zset_count            | Get the number of items within a score range in a sorted set.                                                             |
| zset_incrby           | Increment the score of an item in a sorted set.                                                                           |
| zset_interstore       | Store the intersection of multiple sorted sets into a new sorted set.                                                     |
| zset_lexcount         | Get the count of items within a lexicographical range in a sorted set.                                                    |
| zset_range            | Get items within a rank range from a sorted set.                                                                          |
| zset_lexrange         | Get items within a lexicographical range from a sorted set.                                                               |
| zset_rangebyscore     | Get items within a score range from a sorted set.                                                                         |
| zset_rank             | Get the rank of an item in a sorted set.                                                                                  |
| zset_rem              | Remove one or more items from a sorted set.                                                                               |
| zset_remrangebylex    | Remove items within a lexicographical range from a sorted set.                                                            |
| zset_remrangebyrank   | Remove items within a rank range from a sorted set.                                                                       |
| zset_remrangebyscore  | Remove items within a score range from a sorted set.                                                                      |
| zset_revrange         | Get items within a rank range from a sorted set in reverse order.                                                         |
| zset_revrangebyscore  | Get items within a score range from a sorted set in reverse order.                                                        |
| zset_revrank          | Get the rank of an item in a sorted set in reverse order.                                                                 |
| zset_scan             | Iterate over items in a sorted set.                                                                                      |
| zset_score            | Get the score of an item in a sorted set.                                                                                 |
| zset_unionstore       | Store the union of multiple sorted sets into a new sorted set.                                                            |
