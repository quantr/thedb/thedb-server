%option noyywrap

%{
	enum yytokentype{
		NUMBER=258,
		ADD=259,
		MINUS=260
	};
%}

%%
"+" { return ADD; }
"-" { return MINUS; }
[0-9]+ { return NUMBER; }
%%
