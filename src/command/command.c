#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../grammar/thedbGrammerLib.h"

char *processCommand(char *s) {
  printf("processing:>%s<\n", s);
  if (strcmp(s, "sysinfo") == 0) {
    printf("server: show info");
    char *r = malloc(strlen("{\"name\":\"INTEL 8064\",\"id\":456}") + 1);
    strcpy(r, "{\"name\":\"INTEL 8064\",\"id\":456}");
    return r;
  } else if (strcmp(s, "test") == 0) {
    char *r = malloc(strlen("test乜野呀!!！") + 1);
    strcpy(r, "test乜野呀!!！");
    return r;
  } 
  else if (strcmp(s, "exit") == 0) {
    exit(0);
  }
  else {
    char *test = parseCommand(s);
    // char *r = malloc(strlen("test!!！") + 1);
    // strcpy(r, "test!!！");
    return test;
  }
}