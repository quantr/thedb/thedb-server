#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "thedbGrammar.h"
#include "thedbGrammar.tab.h"
#include "structure/structure.h"

typedef struct yy_buffer_state *YY_BUFFER_STATE;
YY_BUFFER_STATE yy_scan_string(const char *yy_str);
//void yyparse();

struct ast *newast(int nodetype, struct ast *l, struct ast *r) {
  printf("newast nodetype=%d (%c)\n", nodetype, nodetype);
  printf("    l->nodetype=%d (%c)\n", l->nodetype, l->nodetype);
  printf("    r->nodetype=%d (%c)\n", r->nodetype, r->nodetype);
  struct ast *a = malloc(sizeof(struct ast));

  if (!a) {
    yyerror("out of space");
    exit(0);
  }
  a->nodetype = nodetype;
  a->l = l;
  a->r = r;
  return a;
}

struct ast *newnum(double d) {
  struct numval *a = malloc(sizeof(struct numval));

  if (!a) {
    yyerror("out of space");
    exit(0);
  }
  a->nodetype = 'K';
  a->number = d;
  return (struct ast *)a;
}

double eval(struct ast *a) {
  double v;

  switch (a->nodetype) {
    case 'K':
      v = ((struct numval *)a)->number;
      break;

    case '+':
      v = eval(a->l) + eval(a->r);
      break;
    case '-':
      v = eval(a->l) - eval(a->r);
      break;
    case '*':
      v = eval(a->l) * eval(a->r);
      break;
    case '/':
      v = eval(a->l) / eval(a->r);
      break;
    case '|':
      v = eval(a->l);
      if (v < 0) v = -v;
      break;
    case 'M':
      v = -eval(a->l);
      break;
    default:
      printf("internal error: bad node %c\n", a->nodetype);
  }
  return v;
}

void treefree(struct ast *a) {
  switch (a->nodetype) {
      /* two subtrees */
    case '+':
    case '-':
    case '*':
    case '/':
      treefree(a->r);

      /* one subtree */
    case '|':
    case 'M':
      treefree(a->l);

      /* no subtree */
    case 'K':
      free(a);
      break;

    default:
      printf("internal error: free bad node %c\n", a->nodetype);
  }
}

void yyerror(char *s, ...) {
  va_list ap;
  va_start(ap, s);

  fprintf(stderr, "%d: error: ", yylineno);
  vfprintf(stderr, s, ap);
  fprintf(stderr, "\n");
}

int main() 
{
  hashmap_init();
  printf("> ");
  yy_scan_string("str_set miles 1.2");
  yyparse();
  yy_scan_string("str_set miless \"a123\"");
  yyparse();
  yy_scan_string("str_get miless");
  yyparse();
  yy_scan_string("str_set miless \"b246\"");
  yyparse();
  yy_scan_string("str_get miless");
  yyparse();
  yy_scan_string("str_get miles");
  yyparse();

  return 0;
}
