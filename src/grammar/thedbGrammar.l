%option noyywrap nodefault yylineno
%{
#include "grammar/thedbGrammar.h"
#include "../bin/thedbGrammar.tab.h"
%}

/* float exponent */
EXP	([Ee][-+]?[0-9]+)

%%
"+" |
"-" |
"*" |
"/" |
"|" |
"(" |
")" { return yytext[0]; }
[0-9]+"."[0-9]*{EXP}? |
"."?[0-9]+{EXP}? { printf("yytext=\"%s\"\n", yytext); yylval.d = atof(yytext); return NUMBER; }

[a-zA-Z][a-zA-Z0-9]* { printf("yytext=\"%s\"\n", yytext); yylval.s=malloc(strlen(yytext)+1); strcpy(yylval.s, yytext); return KEY; }

["]([^"\\\n]|\\.|\\\n)*["]	{ printf("yytext=\"%s\"\n", yytext); yylval.s=malloc(strlen(yytext)+1); strcpy(yylval.s, yytext); return STRING; }

"str_set" { return STR_SET; }
"str_get" { return STR_GET; }

"str_append" { return STR_APPEND; }
"str_getbit" { return STR_GETBIT; }
"str_getset" { return STR_GETSET; }
"str_mget" { return STR_MGET; }
"str_mset" { return STR_MSET; }
"str_setbit" { return STR_SETBIT; }
"str_strlen" { return STR_STRLEN; }
"str_bitcount" { return STR_BITCOUNT; }
"str_bitop" { return STR_BITOP; }
"str_bitpos" { return STR_BITPOS; }
"str_decr" { return STR_DECR; }
"str_decrby" { return STR_DECRBY; }
"str_getrange" { return STR_GETRANGE; }
"str_setex" { return STR_SETEX; }
"str_setnex" { return STR_SETNEX; }
"str_setrange" { return STR_SETRANGE; }
"str_incr" { return STR_INCR; }
"str_incrby" { return STR_INCRBY; }
"str_incrbyfloat" { return STR_INCRBYFLOAT; }
"str_msetnx" { return STR_MSETNX; }
"str_psetex" { return STR_PSETEX; }



\n      { return EOL; }
"//".*  
[ \t]   { /* ignore white space */ }
.	{ yyerror("Mystery character %c\n", *yytext); }
%%

/*
int main(int argc, char *argv[])
{
  int ret;
  printf("started main in .l\n");
  while ((ret = yylex()) != 0)
  {
    if (ret == STR_SET)
      printf("STR_SET = %s, type = %s\n", yylval.s, ret);
    else if (ret == STR_GET)
      printf("STR_GET = \'\\n\', type = %s\n", ret);
    else
      printf("token = \'%s\', type = %s\n", yytext, ret);
  }
}
*/