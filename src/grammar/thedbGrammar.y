%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "grammar/thedbGrammar.h"
#include "structure/structure.h"
#include "structure/hashmap.h"
#include "structure/dynamicString.h"
int yylex();
extern struct HashMap *mp;
char *returnString;
void *ex_var;
float f;
%}

%union {
  struct ast *a;
  double d;
  char *s;
}

/* declare tokens */
%token <d> NUMBER
%token <s> STR_SET
%token <s> STR_GET

%token <s> STR_APPEND
%token <s> STR_GETBIT
%token <s> STR_GETSET
%token <s> STR_MGET
%token <s> STR_MSET
%token <s> STR_SETBIT
%token <s> STR_STRLEN
%token <s> STR_BITCOUNT
%token <s> STR_BITOP
%token <s> STR_BITPOS
%token <s> STR_DECR
%token <s> STR_DECRBY
%token <s> STR_GETRANGE
%token <s> STR_SETEX
%token <s> STR_SETNEX

%token <s> STR_SETRANGE
%token <s> STR_INCR
%token <s> STR_INCRBY
%token <s> STR_INCRBYFLOAT
%token <s> STR_MSETNX
%token <s> STR_PSETEX

%token <s> KEY
%token <s> STRING
%token EOL

// %type <a> STR_SET KEY

/* %type <a> exp factor term */

%%

thedb: 
| exp EOL {
  printf("> ");
}
| exp {
  printf("> ");
}

exp: 
  STR_SET KEY NUMBER
  { 
    printf("str_set num123 inserting, key = >%s< to map %p \n", $2 , mp); 
    double *d=malloc(sizeof(double));
    *d = $3;
    insert(mp, $2, d, TYPE_FLOAT);
	  returnString=malloc(3);
	  strcpy(returnString, "ok");
  }
  | STR_SET KEY STRING 
  { 
    printf("str_set inserting, key = >%s< to map %p \n", $2 , mp); 
     printf("val str addr:%p\n",  $3);
    insert(mp, $2, $3, TYPE_STRING);
    printf("str_set done\n"); 
	  returnString=malloc(3);
	  strcpy(returnString, "ok");
  }
  | STR_GET KEY 
  { 
    printf("Getting value for key >%s< from map %p \n", $2, mp);
	  returnString = search(mp, $2);
    printf("got %s\n", returnString);
    printf("str_get done\n"); 
  }
  ;

/*
calclist:
| calclist exp EOL {
     printf("$2=%s\n", $2);
     printf("= %4.4g\n", eval($2));
     treefree($2);
     printf("> ");
 }

 | calclist EOL { printf(">> "); }
 ;

exp: factor
 | exp '+' factor { $$ = newast('+', $1,$3); }
 | exp '-' factor { $$ = newast('-', $1,$3);}
 ;

factor: term
 | factor '*' term { $$ = newast('*', $1,$3); }
 | factor '/' term { $$ = newast('/', $1,$3); }
 ;

term: NUMBER   { $$ = newnum($1); }
 | '|' term    { $$ = newast('|', $2, NULL); }
 | '(' exp ')' { $$ = $2; }
 | '-' term    { $$ = newast('M', $2, NULL); }
 ;

*/

%%