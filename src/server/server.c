#include "server.h"

#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../command/command.h"

#define MSG_LIMIT 4096

int startServer(int port) {
  int socket_desc, client_sock, c;
  struct sockaddr_in server, client;

  // Create socket
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_desc == -1) {
    printf("Could not create socket\n");
    return 2;
  }

  // Prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);

  // Bind
  if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0) {
    // print the error message
    perror("bind failed. Error\n");
    return 1;
  }
  printf("listening %d\n", port);

  // Listen
  listen(socket_desc, 3);

  // Accept and incoming connection
  printf("Waiting for incoming connections...\n");
  c = sizeof(struct sockaddr_in);

  // Accept and incoming connection
  c = sizeof(struct sockaddr_in);
  pthread_t thread_id;

  while ((client_sock = accept(socket_desc, (struct sockaddr *)&client,
                               (socklen_t *)&c))) {
    puts("Connection accepted");

    if (pthread_create(&thread_id, NULL, connection_handler,
                       (void *)&client_sock) < 0) {
      perror("could not create thread");
      return 1;
    }

    // Now join the thread , so that we dont terminate before the thread
    // pthread_join( thread_id , NULL);
    printf("Handler assigned\n");
  }

  if (client_sock < 0) {
    perror("accept failed");
    return 1;
  }
  return 0;
}

void *connection_handler(void *socket_desc) {
  printf("\tconnection_handler\n");
  int sock = *(int *)socket_desc;
  int read_size;
  char client_message[MSG_LIMIT + 1];

  while ((read_size = recv(sock, client_message, MSG_LIMIT, 0)) > 0) {
    printf("receiving %d\n", read_size);
    client_message[read_size] = '\0';
    printf("%s\n", client_message);

    char *s = processCommand(client_message);
    write(sock, s, strlen(s));


    memset(client_message, 0, MSG_LIMIT + 1);
  }

  if (read_size == 0) {
    puts("Client disconnected");
    fflush(stdout);
  } else if (read_size == -1) {
    perror("recv failed");
  }
  // Client closed socket so clean up
  close(sock);
  return 0;
}
