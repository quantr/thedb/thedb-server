#ifndef server_h
#define server_h

void *connection_handler(void *socket_desc);
int startServer(int port);

#endif