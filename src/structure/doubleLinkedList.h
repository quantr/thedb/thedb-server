#ifndef doubleLinkedList_h
#define doubleLinkedList_h

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
  int data;
  int key;
  struct node* next;
  struct node* prev;
};

void displayForward();

// display the list from last to first
void displayBackward();

// insert link at the first location
void insertFirst(int key, int data);

// insert link at the last location
void insertLast(int key, int data);

// delete first item
struct node* deleteFirst();

// delete link at the last location
struct node* deleteLast();

// delete a link with given key
struct node* delete(int key);

bool insertAfter(int key, int newKey, int data);

#endif
