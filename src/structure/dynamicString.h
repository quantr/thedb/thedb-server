#ifndef dynamicString_h
#define dynamicString_h

struct dynamicString {
  int len;
  int free;
  char *buffer;
};

void dsInit(struct dynamicString *str, int len);
void dsSet(struct dynamicString *str, char *s);

#endif
