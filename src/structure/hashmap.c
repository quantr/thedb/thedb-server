#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashmap.h"

// like constructor
void setNode(struct node* node, char* key, void* value, int type)
{
	node->key = key;
	node->value = value;
	node->type = type;
	node->next = NULL;
	return;
};

// like constructor
void initializeHashMap(struct HashMap* mp)
{
	// Default capacity in this case
	mp->capacity = 100;
	mp->numOfElements = 0;
	// array of size = 1
	mp->arr = (struct node**)malloc(sizeof(struct node*)
									* mp->capacity);
	return;
}

int hashFunction(struct HashMap* mp, char* key)
{
	int bucketIndex;
	int sum = 0, factor = 31;
	for (int i = 0; i < strlen(key); i++) {
		// sum = sum + (ascii value of
		// char * (primeNumber ^ x))...
		// where x = 1, 2, 3....n
		sum = ((sum % mp->capacity)
			+ (((int)key[i]) * factor) % mp->capacity)
			% mp->capacity;
		// factor = factor * prime
		// number....(prime
		// number) ^ x
		factor = ((factor % __INT16_MAX__)
				* (31 % __INT16_MAX__))
				% __INT16_MAX__;
	}
	bucketIndex = sum;
	return bucketIndex;
}

void insert(struct HashMap* mp, char* key, void* value, int type)
{

	// Getting bucket index for the given
	// key - value pair
	int bucketIndex = hashFunction(mp, key);
	//printf("\tbucketIndex=%d\n", bucketIndex);
	struct node* newNode = (struct node*)malloc(
		// Creating a new node
		sizeof(struct node));
	// Setting value of node
	// printf("in insert fn, type=%d\n", type);
	// printf("in insert fn, val addr:%p\n", value);
	if(type==1){
		printf("in insert fn t1, val=%f\n", *(double *)value);
	}else{
		printf("in insert fn t0, val=%s\n", (char *)value);
	}
	//printf("2 in insert fn t1, val=%f\n", (float *)value);
	setNode(newNode, key, value, type);
	// Bucket index is empty....no collision
	if (mp->arr[bucketIndex] == NULL) {
		mp->arr[bucketIndex] = newNode;
	}

	// Collision
	else {
		// Adding newNode at the head of
		// linked list which is present
		// at bucket index....insertion at
		// head in linked list
		newNode->next = mp->arr[bucketIndex];
		mp->arr[bucketIndex] = newNode;
	}
	return;
}

void delete (struct HashMap* mp, char* key)
{
	// Getting bucket index for the
	// given key
	int bucketIndex = hashFunction(mp, key);
	struct node* prevNode = NULL;
	// Points to the head of
	// linked list present at
	// bucket index
	struct node* currNode = mp->arr[bucketIndex];

	while (currNode != NULL) {

		// Key is matched at delete this
		// node from linked list
		if (strcmp(key, currNode->key) == 0) {
			// Head node
			// deletion
			if (currNode == mp->arr[bucketIndex]) {
				mp->arr[bucketIndex] = currNode->next;
			}
			// Last node or middle node
			else {
				prevNode->next = currNode->next;
			}
			free(currNode);
			break;
		}
		prevNode = currNode;
		currNode = currNode->next;
	}
	return;
}

char* search(struct HashMap* mp, char* key)
{
	// Getting the bucket index
	// for the given key
	int bucketIndex = hashFunction(mp, key);
	//printf("\tbucketIndex=%d\n", bucketIndex);
	// Head of the linked list
	// present at bucket index
	struct node* bucketHead = mp->arr[bucketIndex];
	while (bucketHead != NULL) {
		// Key is found in the hashMap
		if (strcmp(bucketHead->key, key) == 0) {
			//printf("\bucketHead->key=%d\n", bucketHead->key);
			printf("type=%d\n", bucketHead->type);
			if (bucketHead->type==TYPE_STRING){
				return bucketHead->value;
			}else if (bucketHead->type==TYPE_FLOAT){
				char *str=malloc(20);
				printf("1=%f\n", *(double *)bucketHead->value);
    			snprintf(str, 20, "%f", *(double *)bucketHead->value);
				return str;
			}
		}
		bucketHead = bucketHead->next;
	}
	// If no key found in the hashMap
	// equal to the given key
	char* errorMssg = (char*)malloc(sizeof(char) * 25);
	errorMssg = "Oops! No data found.\n";
	return errorMssg;
}

// Drivers code
// int main()
// {

// 	// Initialize the value of mp
// 	struct HashMap* mp
// 		= (struct HashMap*)malloc(sizeof(struct HashMap));
// 	initializeHashMap(mp);

// 	insert(mp, "Yogaholic", "Anjali");
// 	insert(mp, "pluto14", "Vartika");
// 	insert(mp, "elite_Programmer", "Manish");
// 	insert(mp, "GFG", "GeeksforGeeks");
// 	insert(mp, "decentBoy", "Mayank");

// 	printf("%s\n", search(mp, "elite_Programmer"));
// 	printf("%s\n", search(mp, "Yogaholic"));
// 	printf("%s\n", search(mp, "pluto14"));
// 	printf("%s\n", search(mp, "decentBoy"));
// 	printf("%s\n", search(mp, "GFG"));

// 	// Key is not inserted
// 	printf("%s\n", search(mp, "randomKey"));

// 	printf("\nAfter deletion : \n");

// 	// Deletion of key
// 	delete (mp, "decentBoy");
// 	printf("%s\n", search(mp, "decentBoy"));

// 	return 0;
// }
