#ifndef hashmap_h
#define hashmap_h
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TYPE_STRING 0
#define TYPE_FLOAT 1

// Linked List node
struct node {
	// key is string
	char* key;
	// value is also string
	void* value;
	int type;
	struct node* next;
};

// like constructor
void setNode(struct node* node, char* key, void* value, int type);

struct HashMap {

	// Current number of elements in hashMap
	// and capacity of hashMap
	int numOfElements, capacity;

	// hold base address array of linked list
	struct node** arr;
};

// like constructor
void initializeHashMap(struct HashMap* mp);


int hashFunction(struct HashMap* mp, char* key);


void insert(struct HashMap* mp, char* key, void* value, int type);


void delete (struct HashMap* mp, char* key);


char* search(struct HashMap* mp, char* key);

#endif

