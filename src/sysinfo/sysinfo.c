#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../stringbuilder/stringbuilder.h"

char* remove_non_int(char* input) {
  char* ret = (char*) malloc(sizeof(char) * 20);

  int i = 0;
  int j = 0;

  while (input[i] != '\0') {
    if (input[i] >= '0' && input[i] <= '9') {
      ret[j] = input[i];
      j++;
    }
    i++;
  }

  ret[j] = '\0';
  return ret;
}

char* execute_cmd(char* cmd) {
  const unsigned int BUFFER_SIZE = sizeof(char) * 30;
  FILE* fp;
  char* buf = (char*) malloc(BUFFER_SIZE);
  char* ret = (char*) malloc(sizeof(char) * 1000);
  fp = popen(cmd, "r");

  if (fp == NULL) {
    printf("Failed to run command\n");
    return NULL;
  }

  unsigned int len = 0;
  while (fgets(buf, BUFFER_SIZE, fp) != NULL) {
    strcpy(ret + len, buf);
    len += strlen(buf);
  }
  pclose(fp);

  return ret;
}
