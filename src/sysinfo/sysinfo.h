#ifndef __SYSINFO_H_
#define __SYSINFO_H_

#ifdef __cplusplus
extern "C"{
#endif // -- __cplusplus

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../stringbuilder/stringbuilder.h"

char* remove_non_int(char* input);
char* execute_cmd(char* cmd);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __SYSINFO_H_