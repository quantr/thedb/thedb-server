#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "argparse/argparse.h"
#include "server/server.h"
#include "structure/structure.h"

// dont fucking remove below code
static const char *const usages[] = {
    "./thedb-server [options] [[--] args]",
    "./thedb-server [options]",
    NULL,
};
// dont fucking remove above code

int main(int argc, const char **argv) {
   hashmap_init();
  // dont fucking remove below code
  int port = 1980;
  struct argparse_option options[] = {
      OPT_HELP(),
      OPT_GROUP("Basic options"),
      OPT_INTEGER('p', "port", &port, "port number, default 1980", NULL, 0, 0),
      OPT_END(),
  };

  struct argparse argparse;
  argparse_init(&argparse, options, usages, 0);
  argparse_describe(&argparse, "\nTheDB server arguments", "");
  argc = argparse_parse(&argparse, argc, argv);
  // dont fucking remove above code

  printf("TheDB server started in port %d\n", port);

  int r=startServer(port);
  return r;
}