
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gtest/gtest.h>

#include "sysinfo/sysinfo.h"

int main(int argc, char** argv)
{
  // EXPECT_EQ(0, 0);
  // EXPECT_EQ(0, 1);

  char* model_name = execute_cmd("lscpu | grep \"Model name:\"");
  char* archit = execute_cmd("lscpu | grep \"Architecture:\"");
  char* num_cores = execute_cmd("lscpu | grep \"CPU(s):\"");
  char* mhz = execute_cmd("lscpu | grep \"CPU MHz:\"");
  char* mem_total = execute_cmd("cat /proc/meminfo | grep \"MemTotal:\"");
  char* mem_avail = execute_cmd("cat /proc/meminfo | grep \"MemAvailable:\"");

  printf("%s", model_name);
  printf("%s", archit);
  printf("%s", num_cores);
  printf("%s", mhz);
  printf("%s", mem_total);
  printf("%s", mem_avail);

  int int_mem_total = atoi(remove_non_int(mem_total));
  int int_mem_avail = atoi(remove_non_int(mem_avail));

  printf ("Total (MB) : %d. Aval (MB) : %d. Used (MB) : %d \n", int_mem_total/1024, int_mem_avail/1024,  (int_mem_total-int_mem_avail)/1024);
  return 0;
}
